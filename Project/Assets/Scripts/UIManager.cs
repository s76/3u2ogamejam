﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIManager : MonoBehaviour {

   

    public Image healthFill;
    public Image playFill;

    public TextMeshProUGUI score;

    [SerializeField]
    float speechBubbleTime;

    public RectTransform canvasTransform;

    [SerializeField]
    Animator catAnimator;
    [SerializeField]
    Animator speechBubbleAnimator;
    [SerializeField]
    TextMeshProUGUI fatherText;

    Cat cat;

    string[] eatingToFast = new string[] { "Puszki pobaw się nią trochę bardziej!", "Ćwicz swoje umiejętności, z łapki do łapki!",
        " Przed jedzeniem, zawsze zabawa.", "Ja to geeneralnnie potrafiłem się bawić z myszkami. " };

    string[] littleHungry = new string[] {"Masz je łapać a nie zaganiać do dziury.", "Postaraj się trochę bardziej.","Przecież ja Ci jej nie złapie.",
    "Puszek, odrobina wiary w siebie", "To nie są Moto Myszy z Marsa."};

    string[] veryHungry = new string[] { "Tylko nie mów, że jesteś weganinem!", "Zakała kociej rasy.", "Czasami zastanawiam się, czy napewno jesteś moim synem." };

    string[] praise = new string[] { "Świetnie!", "Mama byłaby z Ciebie dumna.", "Mój synek, mysi pogromca!", "Prawie jak tatuś!" };

    string[] introTexts = new string[] {"Puszku, jesteś już dużym kotkiem.", "Czas na lekcję kociej etykiety.", "Zanim zjesz mysz musisz się najpierw z nią podroczyć."};

    bool isDialogActive = false;

    void Start ()
    {
        TutorialFlow();
        cat = GameplayManager.Instance.Cat;
        if (cat != null)
        {
            cat.onHungryValueChanged += OnCatHungryValueChanged;
            cat.onMiceHoldValueChanged += OnCatMiceHoldValueChanged;
            cat.onScoreChanged += OnCatScoreChanged;
            OnCatMiceHoldValueChanged(cat, 0);
            OnCatHungryValueChanged(cat, 1);

        }
    }
    

    void TutorialFlow()
    {
        isDialogActive = true;
        PassTextToFather(introTexts[0]);
        ChangeFatherState(true);
        StartCoroutine(Tutorial(speechBubbleTime));
    }

    private void OnCatMiceHoldValueChanged(Cat cat, float current)
    {
        playFill.fillAmount = current;
        if (current > 0.2f && isDialogActive == false)
        {
            isDialogActive = true;
            int textNumber = UnityEngine.Random.Range(0, eatingToFast.Length);
            PassTextToFather(eatingToFast[textNumber]);
            ChangeFatherState(true);
            StartCoroutine(DialogWiat(speechBubbleTime));
        }
    }

    private void OnCatHungryValueChanged(Cat cat, float current)
    {
        healthFill.fillAmount = current;
        if (current < 0.5f && isDialogActive == false)
        {
            isDialogActive = true;
            if (current < 0.2f)
            {
                int textNumber = UnityEngine.Random.Range(0, veryHungry.Length);
                PassTextToFather(veryHungry[textNumber]);
                ChangeFatherState(true);
                StartCoroutine(DialogWiat(speechBubbleTime));
            }
            else
            {
                int textNumber = UnityEngine.Random.Range(0, littleHungry.Length);
                PassTextToFather(littleHungry[textNumber]);
                ChangeFatherState(true);
                StartCoroutine(DialogWiat(speechBubbleTime));
            }
        }
    }

    private void OnCatScoreChanged(Cat cat, int previous, int current)
    {
        score.text = current.ToString();
    }

    public void ChangeFatherState(bool isDialogueActive)
    {
        catAnimator.SetBool("isFatherActive", isDialogueActive);
        speechBubbleAnimator.SetBool("isSpeechActive", isDialogueActive);
    }
    void PassTextToFather(string textToDisplay)
    {
        fatherText.text = textToDisplay;
    }

    IEnumerator DialogWiat(float time)
    {
        yield return new WaitForSeconds(time);

        ChangeFatherState(false);
        isDialogActive = false;
    }
    IEnumerator Tutorial(float time)
    {
        yield return new WaitForSeconds(time);
        PassTextToFather(introTexts[1]);
        yield return new WaitForSeconds(time);
        PassTextToFather(introTexts[2]);
        yield return new WaitForSeconds(time);
        ChangeFatherState(false);
        isDialogActive = false;
    }

}
