﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : Entity
{
    public delegate void OnHungryValueChanged(Cat cat, float current);
    public delegate void OnMiceHoldValueChanged(Cat cat,float current);
    public delegate void OnScoreChanged(Cat cat, int previous, int current);

    public event OnHungryValueChanged onHungryValueChanged;
    public event OnMiceHoldValueChanged onMiceHoldValueChanged;
    public event OnScoreChanged onScoreChanged;

    [SerializeField]
    private float _miceMaxHoldTime = 5f;

    [SerializeField]
    private float _hungryDecreaseRate = 1f;
    
    [SerializeField]
    private float _hungryMaxValue = 10f;

    [SerializeField]
    private float _hungryRestorePerMice = 4;

    [SerializeField]
    private int _maxMicePlayScore = 10;

    [SerializeField]
    private float _releaseMiceScoreBonus = 2f;

    private Mice _holdingMice;
    private float _lastCaughtMiceTime;
    private float _lastReleaseMiceTime;
    private Mice _lastReleaseMice;
    private float _lastTouchMoveUpdateTime;
    private float _currentHungryValue;

    private float _miceHoldNormalizedValue;
    private int _score;
    private float _lastMiceHoldNormalizedValue;
    private float _lastHungryValue;

    public override bool IsHolding
    {
        get
        {
            return _holdingMice != null;
        }
    }
    private bool ShouldEat {  get { return _miceHoldNormalizedValue == 1; } }
    public Mice HoldingEntity
    {
        get
        {
            return _holdingMice;
        }
        set
        {
            if (_holdingMice == value) return;

            var prev = _holdingMice;
            _holdingMice = value;
            if(_holdingMice != null)
            {
                _lastCaughtMiceTime = Time.time;
                _lastReleaseMice = value;
                _holdingMice.OnBeingCaught(this);
            }else
            {
                var score = Mathf.Lerp(0, _maxMicePlayScore, _miceHoldNormalizedValue + 0.1f);
                if (ShouldEat)
                {
                    prev.OnBeingEaten(this);
                    _currentHungryValue += _hungryRestorePerMice;
                }
                else
                {
                    score *= _releaseMiceScoreBonus;
                    _lastReleaseMiceTime = Time.time;
                    prev.OnBeingReleased(this);
                }

                var prevScore = _score;
                _score += Mathf.RoundToInt(score);
                if(prevScore != _score && onScoreChanged != null)
                {
                    onScoreChanged(this, prevScore, _score);
                }
            }
        }
    }
    protected override void Start()
    {
        base.Start();
        InputManager.Instance.onTouchPhaseChanged += OnTouchPhaseChanged;
        InputManager.Instance.onDoubleTouch += OnDoubleTouch;
        InputManager.Instance.onTouchMoved += OnTouchMoved;
        _lastMiceHoldNormalizedValue = _miceHoldNormalizedValue = 0f;
        _currentHungryValue = _lastHungryValue = _hungryMaxValue;
    }

    private void OnTouchMoved(Vector2 touchPos)
    {
        if (_lastTouchMoveUpdateTime + 0.2f > Time.time) return;
        _lastTouchMoveUpdateTime = Time.time;
        var ray = GameplayManager.Instance.MainCamera.ScreenPointToRay(touchPos);
        Debug.DrawRay(ray.origin, ray.direction * 1000, Color.red);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Default")))
        {
            MoveTo(hit.point);
        }
    }

    protected override void Update()
    {
        base.Update();
        if (IsHolding)
            _miceHoldNormalizedValue = Mathf.Clamp01((Time.time - _lastCaughtMiceTime) / _miceMaxHoldTime);
        else
            _miceHoldNormalizedValue = 0;

        if(Mathf.Abs(_lastMiceHoldNormalizedValue - _miceHoldNormalizedValue) >= 0.001f && onMiceHoldValueChanged != null)
        {
            onMiceHoldValueChanged(this,_miceHoldNormalizedValue);
        }
        _lastMiceHoldNormalizedValue = _miceHoldNormalizedValue;

        if (ShouldEat)
        {
            HoldingEntity = null;
        }

        _currentHungryValue = Mathf.Max(0, _currentHungryValue - _hungryDecreaseRate * Time.deltaTime);
        if (Mathf.Abs(_lastHungryValue - _currentHungryValue) >= 0.001f && onHungryValueChanged != null)
        {
            onHungryValueChanged(this, Mathf.Clamp01(_currentHungryValue/_hungryMaxValue));
        }
        _lastHungryValue = _currentHungryValue;
    }
    private void OnDoubleTouch(Vector2 touchPos)
    {
        var ray = GameplayManager.Instance.MainCamera.ScreenPointToRay(touchPos);
        Debug.DrawRay(ray.origin, ray.direction * 1000, Color.red);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Default")))
        {
            JumpTo(hit.point);
        }
    }

    private void OnTouchPhaseChanged(TouchPhase previous, TouchPhase current, Vector2 touchPos)
    {
        if (current == TouchPhase.Ended)
        {
            if(HoldingEntity != null && _lastCaughtMiceTime + 1 < Time.time)
                HoldingEntity = null;

            var ray = GameplayManager.Instance.MainCamera.ScreenPointToRay(touchPos);
            Debug.DrawRay(ray.origin, ray.direction * 1000, Color.red);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Default")))
            {
                MoveTo(hit.point);
            }
        }
        if (current == TouchPhase.Began)
        {
            var ray = GameplayManager.Instance.MainCamera.ScreenPointToRay(touchPos);
            Debug.DrawRay(ray.origin, ray.direction * 1000, Color.red);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Default")))
            {
                MoveTo(hit.point);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var mice = other.gameObject.GetComponent<Mice>();
        if (mice != null && HoldingEntity == null)
        {
            if (mice == _lastReleaseMice && _lastReleaseMiceTime + 1 > Time.time) return;
            HoldingEntity = mice;
        }
    }
}
