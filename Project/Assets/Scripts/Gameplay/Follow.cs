﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {
    public Transform target;
    public bool ignoreOY;
    private float _origOY;

    private void Start()
    {
        _origOY = transform.position.y;
    }

    private void Update()
    {
        if (target)
        {
            var targ = target.position;
            if (ignoreOY)
            {
                targ.y = _origOY;
            }
            transform.position = Vector3.Lerp(transform.position, targ, 0.35f);
        }
    }
}
