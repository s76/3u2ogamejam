﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIControllerBase : MonoBehaviour
{
    [SerializeField]
    private float _baseupdateInterval = 0.1f;
    private float _lastUpdateTime;

    private IEntity _entity;

    public IEntity Entity {  get { return _entity; } }

    protected void OnEnable()
    {
        _entity = GetComponent<IEntity>();
        _lastUpdateTime = float.NegativeInfinity;
        OnEnableInternal();
    }

    protected virtual void OnEnableInternal()
    {
    }

    protected void Update()
    {
        if (_lastUpdateTime + _baseupdateInterval > Time.time) return;
        _lastUpdateTime = Time.time;
        MainUpdate();
    }

    protected abstract void MainUpdate();
}
