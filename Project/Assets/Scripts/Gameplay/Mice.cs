﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mice : Entity {
    public enum MentalState { Idle, Cautious, Panic, BeingCaught, Dead, InsideBurrow }
    public delegate void OnStateChanged(Mice mice,MentalState previous, MentalState current);

    public event OnStateChanged onStateChanged;

    [Header("Enemey detection parameters")]
    [SerializeField]
    private float _viewAngle;
    [SerializeField]
    private float _viewDistance;
    [SerializeField]
    private float _hearDistance;
    [Header("Mice state parameters")]
    [SerializeField]
    private float _caughtPanicCooldown;
    [SerializeField]
    private float _cautionCooldown;
    [SerializeField]
    private float _speedMultiplierWhenPanic = 1.7f;
    [SerializeField]
    private float _speedMultiplierWhenCautious = 1.35f;
    [SerializeField]
    private Transform spawnAfterDeadPrefab;
    [Header("Burrow settings")]
    [SerializeField]
    private float _burrowTolerance = 2f;
    [SerializeField]
    private float _burrowDetectViewDistanceMultiplier = 1.5f;
    [SerializeField]
    private float _burrowWaitTime = 5f;
    [SerializeField]
    private Transform burrowVfx;


    private MentalState _state;
    private float _lastCaughtTime;
    private float _lastCautiousTime;
    private float _lastBurrowOutTime;
    private IEntity _enemy;
    private bool _isBeingCaught;
    private float _speedMultiplier;
    private Burrow _currentTargetBurrow;
    private float _lastStageChangedTime;
    private float _lastBurrowTime;

    public IEntity Enemy { get { return _enemy; } }

    public MentalState State {
        get { return _state; }
        set
        {
            if(value != _state )
            {
                var prev = _state;
                _state = value;

                OnStateChangedInternal(prev, _state);
            }
        }
    }

    public float ViewAngle { get { return _viewAngle; } }
    public float ViewDistance { get { return _viewDistance; } }
    public float HearDistance { get { return _hearDistance; } }
    public bool IsBeingCaught { get { return _state == MentalState.BeingCaught; } }
    public override float RunAccelerationSpeed { get { return base.RunAccelerationSpeed * _speedMultiplier; } }
    public override float RunSpeed { get { return base.RunSpeed * _speedMultiplier; } }
    public bool CanGoOutOfBurrow { get { return _lastBurrowTime + _burrowWaitTime < Time.time; } }

    public Burrow CurrentTargetBurrow { get { return _currentTargetBurrow; } }

    protected override void Start()
    {
        base.Start();
        _lastCaughtTime = float.MinValue;
        _lastCautiousTime = float.MinValue;
        _lastStageChangedTime = float.MinValue;
        _lastBurrowTime = float.MinValue;
        _speedMultiplier = 1f;
    }

    public void InformAboutEnemyPresence(IEntity enemy)
    {
        _enemy = enemy;
        if(State != MentalState.InsideBurrow)
            State = MentalState.Cautious;
    }

    internal void OnBeingEaten(Cat cat)
    {
        _state = MentalState.Dead;

        View.enabled = false;
        Collider.enabled = false;
        if (spawnAfterDeadPrefab)
        {
            var afterDead = Instantiate(spawnAfterDeadPrefab, this.transform);
            afterDead.localPosition = Vector3.zero;
            afterDead.localScale = Vector3.one;
            afterDead.localRotation = Quaternion.identity;
        }
    }

    public void GoIntoBurrow(Burrow burrow)
    {
        State = MentalState.InsideBurrow;
        if (burrowVfx)
        {
            var vfx = Instantiate(burrowVfx);
            vfx.position = burrow.transform.position;
        }
        _currentTargetBurrow = burrow;
        ClearPath();
    }

    public void OnBeingCaught(IEntity enemy)
    {
        Animator.speed = 2f;
        _enemy = enemy;
        State = MentalState.BeingCaught;
        ClearPath();
    }

    public void OnBeingReleased(IEntity enemy)
    {
        Animator.speed = 1f;
        _state = MentalState.Panic;
        _enemy = enemy;
        _lastCaughtTime = Time.time;
        MoveTo(transform.position);
    }

    protected override void Update()
    {
        if (_state == MentalState.Dead) return;

        if(!IsBeingCaught && _state != MentalState.InsideBurrow)
            base.Update();
        
        switch (_state)
        {
            case MentalState.BeingCaught:
                if (_enemy != null)
                {
                    transform.position = _enemy.Position + _enemy.Forward * 1.3f;
                }
                break;
            case MentalState.InsideBurrow:
                break;
            default:
                if(_lastCaughtTime + _caughtPanicCooldown > Time.time)
                {
                    State = MentalState.Panic;
                }else if(_lastCautiousTime + _cautionCooldown > Time.time)
                {
                    State = MentalState.Cautious;
                } else
                {
                    _enemy = null;
                    State = MentalState.Idle;
                }
                break;
        }

        if(State != MentalState.InsideBurrow && _currentTargetBurrow != null && _lastBurrowOutTime + 5f < Time.time)
        {
            var sqrDis = (_currentTargetBurrow.transform.position - Position).sqrMagnitude;
            if(sqrDis < _burrowTolerance  * _burrowTolerance)
            {
                GoIntoBurrow(_currentTargetBurrow);
            }
        }
    }

    internal void GoOutOfBurrow(Burrow burrow)
    {
        if(burrow != null)
        {
            transform.position = burrow.transform.position;
        }
        _currentTargetBurrow = null;
        State = MentalState.Idle;
        View.enabled = true;
        Collider.enabled = true;
        _lastCaughtTime = float.MinValue;
        _lastCautiousTime = float.MinValue;
        _lastBurrowOutTime = Time.time;
    }

    private void OnStateChangedInternal(MentalState previous, MentalState current)
    {
        switch (previous)
        {
            case MentalState.InsideBurrow:
                View.enabled = true;
                Collider.enabled = true;
                break;
        }

        switch (current)
        {
            case MentalState.Idle:
                _speedMultiplier = 1f;
                break;
            case MentalState.Cautious:
                _speedMultiplier = _speedMultiplierWhenCautious;
                _lastCautiousTime = Time.time;
                break;
            case MentalState.Panic:
                _speedMultiplier = _speedMultiplierWhenPanic;
                break;
            case MentalState.Dead:
                View.enabled = false;
                Collider.enabled = false;
                break;
            case MentalState.InsideBurrow:
                View.enabled = false;
                Collider.enabled = false;
                _lastBurrowTime = Time.time;
                break;
        }

        _lastStageChangedTime = Time.time;
        if( onStateChanged != null)
        {
            onStateChanged(this, previous, _state);
        }
    }

    internal void SetTargetBurrow(Burrow burrow)
    {
        _currentTargetBurrow = burrow;
    }

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
#if UNITY_EDITOR
        var previous = UnityEditor.Handles.color;
        UnityEditor.Handles.color = Color.green * 0.1f;
        if (_currentTargetBurrow != null)
        {
            UnityEditor.Handles.DrawSolidDisc(_currentTargetBurrow.transform.position, Vector3.up, _burrowTolerance);
        }
        UnityEditor.Handles.DrawSolidDisc(transform.position, Vector3.up, _hearDistance);
        var forward = transform.forward;
        var oneEdge = Quaternion.Euler(new Vector3(0, -_viewAngle * 0.5f, 0)) * forward;
        UnityEditor.Handles.color = Color.yellow * 0.25f;
        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, oneEdge, _viewAngle, _viewDistance);
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.up, _viewDistance * _burrowDetectViewDistanceMultiplier );
        UnityEditor.Handles.color = previous;
#endif
    }
}
