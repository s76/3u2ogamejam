﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public delegate void OnTouchPhaseChanged(TouchPhase previous, TouchPhase current, Vector2 touchPos);
    public delegate void OnDoubleTouch(Vector2 touchPos);
    public delegate void OnTouchMoved(Vector2 touchPos);

    public event OnTouchPhaseChanged onTouchPhaseChanged;
    public event OnDoubleTouch onDoubleTouch;
    public event OnTouchMoved onTouchMoved;

    [SerializeField]
    private float _doubleTapTimeThreshold = 0.75f;
    [SerializeField]
    private float _doubleTapTimeTolerance= 40f;
    [SerializeField]
    private float _doubleTapCooldown = 0.2f;
    private float _lastDoubleTouchTime;
    private TouchPhase _lastTouchPhase;
    private float _lastTouchBeginTime;
    private Vector2 _lastTouchPosition;

    private void Update()
    {
        Touch touch;
        if(GetTouch(out touch))
        {
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (_lastTouchBeginTime + _doubleTapTimeThreshold > Time.time
                        && _lastDoubleTouchTime + _doubleTapCooldown < Time.time)
                    {
                        if((touch.position - _lastTouchPosition).sqrMagnitude < _doubleTapTimeTolerance * _doubleTapTimeTolerance)
                        {
                            if (onDoubleTouch != null)
                            {
                                onDoubleTouch(touch.position);
                            }
                            _lastDoubleTouchTime = Time.time;
                        }
                    }
                    _lastTouchBeginTime = Time.time;
                    break;
                case TouchPhase.Canceled:
                case TouchPhase.Ended:
                    break;
                case TouchPhase.Moved:
                    if(onTouchMoved != null)
                    {
                        onTouchMoved(touch.position);
                    }
                    break;
                case TouchPhase.Stationary:
                    break;
            }

            if (_lastTouchPhase != touch.phase && onTouchPhaseChanged != null)
            {
                onTouchPhaseChanged(_lastTouchPhase, touch.phase, touch.position);
            }
            _lastTouchPhase = touch.phase;
            _lastTouchPosition = touch.position;
        }
    }
    public bool GetTouch(out Touch touch)
    {
#if !UNITY_EDITOR
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            return true;
        }
        else
        {
            touch = default(Touch);
            return false;
        }
#else
        Touch lastFakeTouch = new Touch();
        if (Input.GetMouseButtonDown(0))
        {
            lastFakeTouch.phase = TouchPhase.Began;
            lastFakeTouch.deltaPosition = new Vector2(0, 0);
            lastFakeTouch.position = Input.mousePosition;
            lastFakeTouch.fingerId = 0;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            lastFakeTouch.phase = TouchPhase.Ended;
            Vector2 newPosition = Input.mousePosition;
            lastFakeTouch.deltaPosition = newPosition - lastFakeTouch.position;
            lastFakeTouch.position = newPosition;
            lastFakeTouch.fingerId = 0;
        }
        else if (Input.GetMouseButton(0))
        {
            Vector2 newPosition = Input.mousePosition;
            var delta = newPosition - lastFakeTouch.position;
            lastFakeTouch.phase = delta.magnitude < 1f ? TouchPhase.Stationary: TouchPhase.Moved; 
            lastFakeTouch.deltaPosition = delta;
            lastFakeTouch.position = newPosition;
            lastFakeTouch.fingerId = 0;
        }
        else
        {
            touch = default(Touch);
            return false;
        }
        touch = lastFakeTouch;
        return true;
#endif
    }
}
