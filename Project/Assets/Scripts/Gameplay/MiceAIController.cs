﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class MiceAIController : AIControllerBase
{
    [SerializeField]
    private float _idleUpdateInterval = 4f;
    [SerializeField]
    private float _cautiousUpdateInterval = 2f;
    [SerializeField]
    private float _panicUpdateInterval = 1f;

    private float _lastIdleUpdateTime;
    private float _lastCautiousUpdateTime;
    private float _lastPanicUpdateTime;
    private Mice _mice;
    private Vector3 _enemyLastKnownLocation;

    protected override void OnEnableInternal()
    {
        base.OnEnableInternal();
        _mice = Entity as Mice;
        _mice.onStateChanged += OnStateChanged;
        _lastIdleUpdateTime = float.MinValue;
        _lastCautiousUpdateTime = float.MinValue;
        _lastPanicUpdateTime = float.MinValue;
    }

    private void OnDestroy()
    {
        if(_mice != null)
        {
            _mice.onStateChanged -= OnStateChanged;
        }
    }

    private void OnStateChanged(Mice mice, Mice.MentalState previous, Mice.MentalState current)
    {
        switch(current)
        {
            case Mice.MentalState.InsideBurrow:
                _lastPanicUpdateTime = float.MinValue;
                _lastCautiousUpdateTime = float.MinValue;
                break;
        }
    }

    protected override void MainUpdate()
    {
        if (_mice == null) return;
        if (_mice.State == Mice.MentalState.Dead) return;

        if (_mice.IsBeingCaught) return;

        if (_mice.Enemy != null)
            _enemyLastKnownLocation = _mice.Enemy.Position;

        if (GameplayManager.Instance.Cat != null)
        {
            var enemy = GameplayManager.Instance.Cat;
            var toEnemy = enemy.Position - _mice.Position;
            var distanceToEnemy = toEnemy.magnitude;
            var toEnemyDir = toEnemy / distanceToEnemy;
            var forward = _mice.transform.forward;
            var dot = Vector3.Dot(toEnemyDir, forward);
            // inside field of view
            if (dot > Mathf.Cos(_mice.ViewAngle * 0.5f * Mathf.Deg2Rad))
            {
                NavMeshHit hit;
                if (!NavMesh.Raycast(_mice.Position, enemy.Position, out hit,NavMesh.AllAreas))
                    _mice.InformAboutEnemyPresence(enemy);
            }
            else if (distanceToEnemy <= _mice.HearDistance)
            {
                if (enemy.IsMakingNoise)
                    _mice.InformAboutEnemyPresence(enemy);
                else if (distanceToEnemy < _mice.HearDistance * 0.5f)
                    _mice.InformAboutEnemyPresence(enemy);
            }
        }

        switch (_mice.State)
        {
            case Mice.MentalState.Idle:
                if (_lastIdleUpdateTime + _idleUpdateInterval > Time.time) return;
                _lastIdleUpdateTime = Time.time;

                var randAngle = UnityEngine.Random.Range(-60, 60);
                var dir = Quaternion.Euler(0, randAngle, 0) * transform.forward;

                _mice.MoveTo(dir * 5);
                
                break;
            case Mice.MentalState.Cautious:
                if (_lastCautiousUpdateTime + _cautiousUpdateInterval > Time.time) return;
                _lastCautiousUpdateTime = Time.time;

                if (_mice.Enemy == null)
                    MoveAwayFrom(_enemyLastKnownLocation);
                else MoveAwayFrom(_mice.Enemy.Position);

                break;
            case Mice.MentalState.Panic:
                if (_lastPanicUpdateTime + _panicUpdateInterval > Time.time) return;
                _lastPanicUpdateTime = Time.time;

                if (_mice.Enemy == null)
                    MoveAwayFrom(_enemyLastKnownLocation);
                else
                    MoveAwayFrom(_mice.Enemy.Position);
                break;
            case Mice.MentalState.InsideBurrow:
                if(_mice.CanGoOutOfBurrow)
                {
                    if (_mice.Enemy == null)
                        _mice.GoOutOfBurrow(_mice.CurrentTargetBurrow);
                    else
                    {
                        var enemyClose = (_mice.Enemy.Position - _mice.Position).sqrMagnitude < _mice.ViewDistance * _mice.ViewDistance;
                        if(enemyClose)
                        {
                            var burrows = GameplayManager.Instance.BurrowsList.Where(x => x != _mice.CurrentTargetBurrow).ToArray();
                            var newBurrow = burrows[UnityEngine.Random.Range(0, burrows.Length)];
                            _mice.GoOutOfBurrow(newBurrow);
                        } else
                        {
                            _mice.GoOutOfBurrow(_mice.CurrentTargetBurrow);
                        }
                    }
                } 
                break;
        }
        
    }

    private void MoveAwayFrom(Vector3 enemyPosition)
    {
        var toEnemy = enemyPosition - _mice.Position;
        var distanceToEnemy = toEnemy.magnitude;
        var toEnemyDir = toEnemy/ distanceToEnemy;
        var finalDir = -toEnemyDir;

        var randAngle = UnityEngine.Random.Range(-20, 20);
        var burrow = GetBestBurrowPosition(GameplayManager.Instance.BurrowsList,_mice.Position, enemyPosition);
        if(burrow != null)
        {
            var toBurrowDir = (burrow.transform.position - _mice.Position).normalized;
            finalDir += toBurrowDir;
        }
        finalDir = Quaternion.Euler(0, randAngle, 0) * finalDir;
        var targetPos = finalDir * _mice.HearDistance * UnityEngine.Random.Range(1f,2f);
        _mice.SetTargetBurrow(burrow);
        _mice.MoveTo(targetPos);
    }

    private Burrow GetBestBurrowPosition(List<Burrow> burrowsList, Vector3 micePosition, Vector3 enemyPosition)
    {
        Burrow[] bestResults = new Burrow[3];
        float[] bestDistance = new float[3] { float.MaxValue -2, float.MaxValue - 1, float.MaxValue};
        var sortedByDis = burrowsList.OrderBy(x => (x.transform.position - micePosition).sqrMagnitude).Take(3);

        var sortedByDot = sortedByDis.OrderBy(x =>
        {
            var toEnenemy = (enemyPosition - _mice.Position).normalized;
            var toBurrow = (x.transform.position - _mice.Position).normalized;
            return Vector3.Dot(toEnenemy, toBurrow);
        });
        return sortedByDot.FirstOrDefault();
    }
}
