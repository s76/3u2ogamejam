﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public interface IEntity
{
    Vector3 Position { get; }
    Vector3 Forward { get; }
    bool IsMakingNoise { get; }
    bool IsHolding { get; }

    void MoveTo(Vector3 targetPosition );
    void JumpTo(Vector3 targetPosition);
    void ClearPath();
}

public class Entity : MonoBehaviour ,IEntity
{
    public delegate void OnEntityStart(Entity entity);
    public static event OnEntityStart onEntityStart;

    public enum AnimState { Idle, Run , Jump , Play, Eat }

    [SerializeField]
    private AnimState _animationState;

    [Header("Basic entity settings")]
    [SerializeField]
    private Renderer _view;
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private Collider _collider;
    [SerializeField]
    private NavMeshAgent _navMeshAgent;
    [Header("Basic entity parameters")]
    [SerializeField]
    private float _runSpeed;
    [SerializeField]
    private float _runAccelerationSpeed;
    [SerializeField]
    private float _jumpSpeed;
    [SerializeField]
    private float _jumpAccelerationSpeed;
    [SerializeField]
    private float _jumpMaxDistance;

    private float _currentMoveSpeed;
    private Vector3 _lastJumpTargetPosition;
    private float _currentJumpTravelDistance;
    private int _currentCornerIndex;
    private Vector3 _lastTargetPosition;
    private NavMeshPath _path;
    private bool _isJumping;

    public Vector3 Position { get { return transform.position; } }
    public Vector3 Forward { get { return transform.forward; } }
    public Renderer View { get { return _view; } }
    public Collider Collider { get { return _collider; } }
    public Animator Animator { get { return _animator; } }
    public virtual float RunSpeed {  get { return _runSpeed; } }
    public virtual float RunAccelerationSpeed { get { return _runAccelerationSpeed; } }
    public bool IsMakingNoise { get { return _currentMoveSpeed > 0; } }
    public AnimState AnimationState
    {
        get { return _animationState; }
        set
        {
            if(value != _animationState)
            {
                _animationState = value;
                if(_animator)
                    _animator.SetInteger("AnimationState", (int)value);
            }
        }
    }

    public virtual bool IsHolding {  get { return false; } }

    protected virtual void Start()
    {
        _path = new NavMeshPath();
        if (onEntityStart != null)
            onEntityStart(this);
    }
  
    public void JumpTo(Vector3 targetPosition)
    {
        if (_isJumping) return;
        var direction = (targetPosition - transform.position).normalized;
        targetPosition = transform.position + direction * _jumpMaxDistance;
        NavMeshHit hit;
        if (NavMesh.Raycast(Position, targetPosition, out hit, NavMesh.AllAreas))
        {
            targetPosition = hit.position;
        }
        if ((targetPosition - transform.position).sqrMagnitude < 1f)
            return;

        _isJumping = true;
        AnimationState = AnimState.Jump;
        ClearPath();

        _navMeshAgent.CalculatePath(targetPosition, _path);
        _lastJumpTargetPosition = targetPosition;
        _currentJumpTravelDistance = (targetPosition - transform.position).magnitude;
    }

    protected virtual void Update()
    {
        var corners = _path.corners;
        if (_path.status != NavMeshPathStatus.PathInvalid)
        {
            var acceleration = _isJumping ? _jumpAccelerationSpeed : RunAccelerationSpeed;
            var maxSpeed = _isJumping ? _jumpSpeed : RunSpeed;

            _currentMoveSpeed += acceleration * Time.deltaTime;
            _currentMoveSpeed = Mathf.Min(_currentMoveSpeed, maxSpeed);

            var travelDistance = _currentMoveSpeed * Time.deltaTime;
            Vector3 direction;
            Vector3 destinationPoint = transform.position;

            if (!_isJumping)
            {
                var previousCorner = corners[_currentCornerIndex];
                var realPassedLengthOnPath = travelDistance+
                    + (transform.position - corners[_currentCornerIndex]).magnitude;

                var currentIdx = _currentCornerIndex;
                
                while (true)
                {
                    if (currentIdx + 1 >= corners.Length)
                    {
                        direction = transform.forward;
                        destinationPoint = corners[currentIdx];
                        break;
                    }
                    var current = corners[currentIdx];
                    var next = corners[currentIdx + 1];
                    var pointDelta = next - current;
                    pointDelta.y = 0;
                    var distanceBetweenPoints = pointDelta.magnitude;
                    if (realPassedLengthOnPath >= distanceBetweenPoints)
                    {
                        realPassedLengthOnPath -= distanceBetweenPoints;
                        currentIdx++;
                    }
                    else
                    {
                        var part = realPassedLengthOnPath / distanceBetweenPoints;
                        direction = pointDelta.normalized;
                        destinationPoint = Vector3.Lerp(current, next, part);
                        break;
                    }
                }
                _currentCornerIndex = currentIdx;

                if (_currentCornerIndex == corners.Length - 1)
                {
                    _currentMoveSpeed = 0;
                }
            } else
            {
                if(_currentJumpTravelDistance < travelDistance)
                {
                    travelDistance = _currentJumpTravelDistance;
                }

                _currentJumpTravelDistance -= travelDistance;
                _isJumping = _currentJumpTravelDistance > 0;
                direction = (_lastJumpTargetPosition - transform.position).normalized;
                destinationPoint += travelDistance * direction;
            }
            
            transform.position = destinationPoint;
            direction.y = transform.forward.y;
            transform.forward = Vector3.Slerp(transform.forward,direction, 0.35f);
            if (_currentMoveSpeed > 0)
                AnimationState = _isJumping ? AnimState.Jump : AnimState.Run;
            else
                AnimationState = IsHolding ? AnimState.Play : AnimState.Idle;
        } else
        {
            _isJumping = false;
            AnimationState = IsHolding ? AnimState.Play : AnimState.Idle;
        }
    }

    public void MoveTo(Vector3 targetPosition)
    {
        if (_isJumping) return;
        NavMeshHit hit; 
        if (!NavMesh.SamplePosition(targetPosition, out hit, 10, NavMesh.AllAreas)) return;
        _navMeshAgent.CalculatePath(hit.position, _path);
        _currentCornerIndex = 0;
        _lastTargetPosition = hit.position;
    }
    
    protected virtual void OnDrawGizmosSelected()
    {
        if (_path == null) return;
        var corners = _path.corners;
        var prevColor = Gizmos.color;
        for(int i = 0; i < corners.Length; i++)
        {
            if(i == _currentCornerIndex )
            {
                Gizmos.color = Color.yellow;
            }
            Gizmos.DrawSphere(corners[i], 0.2f);
            if (i == _currentCornerIndex)
            {
                Gizmos.color = prevColor;
            }
        }

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_lastTargetPosition, 0.5f);
        Gizmos.color = prevColor;
    }

    public void ClearPath()
    {
        _path.ClearCorners();
        _currentCornerIndex = 0;
    }
}
