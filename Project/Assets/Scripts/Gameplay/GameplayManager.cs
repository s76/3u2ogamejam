﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : Singleton<GameplayManager>
{
    public Cat Cat  { get; private set; }
    public List<Mice> MiceList { get { return _miceList; } }
    public List<Burrow> BurrowsList { get { return _burrowsList; } }
    public Camera MainCamera { get { return Camera.main; } }

    private List<Burrow> _burrowsList = new List<Burrow>();
    private List<Mice> _miceList = new List<Mice>();
    private List<Mice> _bufferMiceList = new List<Mice>();

    [SerializeField]
    private Mice micePrefab;

    private void Start()
    {
        SceneManager.LoadScene("UI", LoadSceneMode.Additive);
        StartCoroutine(DelayedSpawn(3f));
    }

    private IEnumerator DelayedSpawn(float delay)
    {
        yield return new WaitForSeconds(delay);
        SpawnMice(5);
    }

    internal void RegisterBurrow(Burrow burrow)
    {
        if (!_burrowsList.Contains(burrow))
            _burrowsList.Add(burrow);
    }
    
    public void SpawnMice(int number)
    {
        StartCoroutine(SpawnMiceWithDelay(number, _bufferMiceList, 0.1f));
    }

    private IEnumerator SpawnMiceWithDelay(int number, List<Mice> bufferMiceList, float delay)
    {
        _bufferMiceList.Clear();
        while (number-- > 0)
        {
            var mice = Instantiate(micePrefab);
            _bufferMiceList.Add(mice);
            if (BurrowsList.Count > 0)
            {
                var rand = BurrowsList[UnityEngine.Random.Range(0, BurrowsList.Count)];
                mice.transform.position = rand.transform.position;
            }
            mice.onStateChanged += OnStateChanged;
            yield return new WaitForSeconds(delay);
        }
        _miceList.AddRange(bufferMiceList);
    }

    private void OnStateChanged(Mice mice, Mice.MentalState previous, Mice.MentalState current)
    {
        if(current == Mice.MentalState.Dead)
        {
            SpawnMice(1);
            mice.onStateChanged -= OnStateChanged;
        }
    }

    protected override void AwakeInternal()
    {
        Entity.onEntityStart += OnEntityStart;
        base.AwakeInternal();
    }
    protected override void OnDestroyInternal()
    {
        base.OnDestroyInternal();
        Entity.onEntityStart -= OnEntityStart;
    }

    private void OnEntityStart(Entity entity)
    {
        if (entity is Cat)
        {
            Cat = entity as Cat;
        } 
    }
}
