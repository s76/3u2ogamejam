﻿using UnityEngine;
using System.Collections;
using System;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance { get; private set; }

    protected void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this as T;
        AwakeInternal();
    }

    protected virtual void AwakeInternal()
    {
    }

    protected void OnDestroy()
    {
        OnDestroyInternal();
        Instance = null;
    }

    protected virtual void OnDestroyInternal()
    {
    }
}