﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    Vector3 catPosition;
    List<Vector3> micePositions = new List<Vector3>();
    List<Vector3> miceToCatDiff = new List<Vector3>();

    public float minimalDistance;
    [SerializeField]
    SpriteRenderer arrow;
    Vector3 topRight;
    [SerializeField]
    Animator animator;

    private void Start()
    {
    }

    void Update ()
    {
        catPosition = GameplayManager.Instance.Cat.Position;
        float bestDis = float.MaxValue;
        Mice bestMice = null;
        foreach (Mice  mice in GameplayManager.Instance.MiceList)
        {
            if (mice.State == Mice.MentalState.Dead || mice.State == Mice.MentalState.InsideBurrow) continue;
            var dis = (mice.Position - catPosition).sqrMagnitude;
            if(dis < bestDis)
            {
                bestDis = dis;
                bestMice = mice;
            }
        }
        
        if(bestMice != null && bestDis > minimalDistance)
        {
            //arrow.enabled = true;
            animator.SetBool("isArrowVisible", true);
            Vector3 diff = catPosition - bestMice.Position;
            diff.Normalize();
            float rot_y = Mathf.Atan2(diff.z, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, -rot_y - 90, 0f);
        }
        else
        {
            //arrow.enabled = false;
            animator.SetBool("isArrowVisible", false);
        }
    }
}
